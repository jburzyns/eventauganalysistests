# Setup
```
git clone https://:@gitlab.cern.ch:8443/jburzyns/eventauganalysistests.git
cd eventauganalysistests
mkdir build run
cd build
asetup Athena,master,latest
cmake ../source
make
source x86_64-centos7-gcc11-opt/setup.sh
```
# Run
```
cd ../run
athena AugmentationTest/AugmentationTestAlgJobOptions.py
```
