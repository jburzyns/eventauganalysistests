from glob import glob
jps.AthenaCommonFlags.FilesInput = glob("/eos/user/j/jburzyns/public/forNils/DAOD_PHYS_LLP1.output.test.pool.root") #must happen before the import of the read file mode
jps.AthenaCommonFlags.AccessMode = "POOLAccess" 

from EventSelectorAthenaPool.EventSelectorAthenaPoolConf import StreamSelectorTool
selector = StreamSelectorTool()
selector.AcceptStreams = "StreamDAOD_LLP1"
svcMgr.EventSelector.HelperTools += [ selector ]
svcMgr += CfgMgr.AthenaPoolAddressProviderSvc ("AthenaPoolAddressProviderSvc_LLP1")
svcMgr.ProxyProviderSvc.ProviderNames += [
"AthenaPoolAddressProviderSvc/AthenaPoolAddressProviderSvc_LLP1" ]
svcMgr.AthenaPoolAddressProviderSvc_LLP1.DataHeaderKey = "StreamDAOD_LLP1"

# add alg to the sequence
athAlgSeq += CfgMgr.AugmentationTestAlg()  

include("AthAnalysisBaseComps/SuppressLogging.py")              
