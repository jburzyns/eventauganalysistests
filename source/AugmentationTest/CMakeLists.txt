## automatically generated CMakeLists.txt file

# Declare the package
atlas_subdir( AugmentationTest )

# Declare external dependencies ... default here is to include ROOT
find_package( ROOT COMPONENTS MathCore RIO Core Tree Hist )

# Declare package as a library
# Note the convention that library names get "Lib" suffix
# Any package you depend on you should add
# to LINK_LIBRARIES line below (see the example)
atlas_add_library( AugmentationTestLib src/*.cxx
#                   PUBLIC_HEADERS AugmentationTest
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES ${ROOT_LIBRARIES}
                                    AthAnalysisBaseCompsLib xAODEventInfo xAODTracking xAODCaloEvent AsgAnalysisInterfaces
)

# if you add athena components (tools, algorithms) to this package
# these lines are needed so you can configure them in joboptions
atlas_add_component( AugmentationTest src/components/*.cxx
                      NOCLIDDB
                      LINK_LIBRARIES AugmentationTestLib xAODTracking 
)

# Install python modules, joboptions, and share content
atlas_install_joboptions( share/*.py )
