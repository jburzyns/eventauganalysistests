#include "AugmentationTestAlg.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCaloEvent/CaloClusterContainer.h"


AugmentationTestAlg::AugmentationTestAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){


}

AugmentationTestAlg::~AugmentationTestAlg() {}


StatusCode AugmentationTestAlg::initialize() {
  return StatusCode::SUCCESS;
}

StatusCode AugmentationTestAlg::finalize() {
  return StatusCode::SUCCESS;
}

StatusCode AugmentationTestAlg::execute() {
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ATH_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  // print out run and event number from retrieved object
  if(m_eventCount % 1000 == 0) {
    ATH_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
  }
  m_eventCount++;

  // retrieve the CaloClusterContainer from the store
  const xAOD::CaloClusterContainer *caloClusterContainer = nullptr;
  ATH_CHECK (evtStore()->retrieve (caloClusterContainer, "CaloCalTopoClusters"));

  for(const xAOD::CaloCluster *caloCell : *caloClusterContainer) {
    ATH_MSG_INFO(caloCell->calEta());
  }

  return StatusCode::SUCCESS;
}


StatusCode AugmentationTestAlg::beginInputFile() { 

  return StatusCode::SUCCESS;
}


