#ifndef LRTUNCERTAINTIES_LRTUNCERTAINTIESALG_H
#define LRTUNCERTAINTIES_LRTUNCERTAINTIESALG_H

// Athena Includes
#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

class AugmentationTestAlg: public ::AthAnalysisAlgorithm { 
 public: 
  AugmentationTestAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~AugmentationTestAlg(); 

  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  virtual StatusCode  execute();        //per event
  virtual StatusCode  finalize();       //once, after all events processed
  

 private: 
   int m_eventCount = 0;

}; 

#endif //> !LRTUNCERTAINTIES_LRTUNCERTAINTIESALG_H
